const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const app = express();
const axios = require("axios");
const CircularJSON = require("circular-json");

app.use(express.static(path.join(__dirname, "build")));

// Body parser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// CORS
app.all("/*", (req, res, next) => {
	res.set("Access-Control-Allow-Origin", "*");
	res.set("Access-Control-Allow-Methods", "*");
	res.set(
		"Access-Control-Allow-Headers",
		"Origin, X-Requested-With, Content-Type, Accept"
	);
	next();
});

const baseUrl =
	"https://us-central1-rival-chatbot-challenge.cloudfunctions.net";

// API ENDPOINTS //
app.post("/api-register-user", (req, res) => {
	const { name, email } = req.body;
	axios({
		method: "post",
		url: `${baseUrl}/challenge-register`,
		data: {
			name,
			email,
		},
	})
		.then((response) => {
			res.status(200).send(response.data);
		})
		.catch((error) => {
			console.log(error);
			res.status(400).send("Could not register");
		});
});

app.post("/api-challenge-conversation", (req, res) => {
	const { user_id } = req.body;
	axios({
		method: "post",
		url: `${baseUrl}/challenge-conversation`,
		data: {
			user_id,
		},
	})
		.then((response) => {
			res.status(200).send(response.data);
		})
		.catch((error) => {
			console.log(error);
			// res.status(400).send("Could not challenge conversation");
		});
});

app.get("/api-challenge-behaviour", (req, res) => {
	const { conversation_id } = req.query;
	axios
		.get(`${baseUrl}/challenge-behaviour/${conversation_id}`)
		.then((response) => {
			res.status(200).send(response.data);
		})
		.catch((error) => {
			console.log(error);
			res.status(400).send("Could not get message");
		});
});

app.post("/api-challenge-behaviour", (req, res) => {
	const { conversation_id, content } = req.body;
	axios({
		method: "post",
		url: `${baseUrl}/challenge-behaviour/${conversation_id}`,
		data: {
			content,
		},
	})
		.then((response) => {
			res.status(200).send(response.data);
		})
		.catch((error) => {
			console.log(error);
			res.status(400).send("Could not reply");
		});
});

// FRONT END //
app.get("/", (req, res) => {
	res.sendFile(path.join(__dirname, "build", "index.html"));
});

app.listen(8080, () => {
	console.log("server started on port 8080");
});
