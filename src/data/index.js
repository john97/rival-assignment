import axios from "axios";

const baseUrl = "http://localhost:8080";

export const registerUser = async (name, email) => {
	try {
		const res = await axios({
			method: "post",
			url: `${baseUrl}/api-register-user`,
			data: {
				name,
				email,
			},
		});
		return res;
	} catch (err) {
		console.log(err);
	}
};

export const startConversation = async (user_id) => {
	try {
		const res = await axios({
			method: "post",
			url: `${baseUrl}/api-challenge-conversation`,
			headers: {
				"Content-Type": "application/json",
			},
			data: {
				user_id,
			},
		});
		return res;
	} catch (err) {
		console.log(err);
	}
};

export const sendReply = async (conversation_id, content) => {
	try {
		const res = await axios({
			method: "post",
			url: `${baseUrl}/api-challenge-behaviour`,
			headers: {
				"Content-Type": "application/json",
			},
			data: {
				conversation_id,
				content,
			},
		});
		return res;
	} catch (err) {
		console.log(err);
	}
};

export const getNextMessage = async (conversation_id) => {
	try {
		const res = await axios.get(`${baseUrl}/api-challenge-behaviour`, {
			params: {
				conversation_id,
			},
		});
		return res;
	} catch (err) {
		console.log(err);
	}
};
