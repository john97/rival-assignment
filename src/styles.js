import styled from "styled-components";
import { Container, Form } from "reactstrap";

export const SContainer = styled(Container)`
	padding-top: 20px;
`;

export const SFormContainer = styled.div`
	padding: 15px;
	border-radius: 10px;
	background-color: #f2f2f2;
`;

export const SForm = styled(Form)``;
