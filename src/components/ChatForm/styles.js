import styled from "styled-components";

export const SConvoContainer = styled.div`
	padding: 10px;
	background-color: #fff;
`;

export const SMessagesContainer = styled.div`
	min-height: 120px;
`;

export const SMessageContainer = styled.div`
	padding: 8px;
	border: 1px solid #000;
	border-radius: 5px;
`;
