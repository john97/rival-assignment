import React, { useEffect, useState } from "react";
import {
	Form,
	Label,
	Input,
	FormGroup,
	Button,
	Row,
	Col,
	Alert,
} from "reactstrap";
import axios from "axios";

import { sendReply, getNextMessage } from "../../data";
import {
	SConvoContainer,
	SMessagesContainer,
	SMessageContainer,
} from "./styles";

const RegisterForm = ({ conversation_id }) => {
	const [userMessage, setUserMessage] = useState("");
	const [chatbotMessage, setChatbotMessage] = useState("");
	const [getMessageError, setGetMessageError] = useState(false);
	const [sendMessageError, setSendMessageError] = useState(false);
	const [wrongAnswer, setWrongAnswer] = useState(false);

	useEffect(() => {
		getMessage(conversation_id);
	}, []);

	const getMessage = async (conversation_id) => {
		const result = await getNextMessage(conversation_id);
		if (result && result.status === 200) {
			const messages = result.data.messages;
			const lastMessage = messages[messages.length - 1];
			setChatbotMessage(lastMessage);
		} else {
			setGetMessageError(true);
		}
	};

	const reply = async () => {
		const result = await sendReply(conversation_id, userMessage);
		if (result && result.status === 200) {
			const correct = result.data.correct;
			if (correct) {
				setWrongAnswer(false);
				getMessage(conversation_id);
			} else {
				setWrongAnswer(true);
			}
		} else {
			setSendMessageError(true);
		}
		setUserMessage("");
	};

	return (
		<SConvoContainer>
			<SMessagesContainer>
				<SMessageContainer>{chatbotMessage.text}</SMessageContainer>
			</SMessagesContainer>
			<Form>
				<Row form>
					<Col md={9}>
						<FormGroup>
							<Input
								type="text"
								name="user-message"
								id="user-message"
								placeholder="Your reply"
								onChange={(e) => setUserMessage(e.target.value)}
								value={userMessage}
							/>
						</FormGroup>
					</Col>
					<Col md={3} style={{ width: "100%" }}>
						<Button onClick={reply}>Reply</Button>
					</Col>
				</Row>
				{getMessageError ? (
					<Alert color="danger" style={{ marginTop: 10 }}>
						Error: Could not retrieve message
					</Alert>
				) : sendMessageError ? (
					<Alert color="danger" style={{ marginTop: 10 }}>
						Error: Could not send message
					</Alert>
				) : null}
				{wrongAnswer && (
					<Alert color="warning" style={{ marginTop: 10 }}>
						Wrong answer: Try again!
					</Alert>
				)}
			</Form>
		</SConvoContainer>
	);
};

export default RegisterForm;
