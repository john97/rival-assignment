import React, { useEffect, useState } from "react";
import { Form, Label, Input, FormGroup, Button, Alert } from "reactstrap";

import { registerUser, startConversation } from "../../data";

import ChatForm from "../ChatForm";

const RegisterForm = () => {
	const [name, setName] = useState("");
	const [email, setEmail] = useState("");
	const [conversationId, setConversationId] = useState("");
	const [initSuccess, setInitSuccess] = useState(false);
	const [error, setError] = useState(false);

	const register = async () => {
		const result = await registerUser(name, email);
		if (result && result.status === 200) {
			const user_id = result.data.user_id;
			initConversation(user_id);
		} else {
			setError(true);
		}
	};

	const initConversation = async (user_id) => {
		const result = await startConversation(user_id);
		if (result && result.status === 200) {
			const conversation_id = result.data.conversation_id;
			setConversationId(conversation_id);
			setInitSuccess(true);
		} else {
			setError(true);
		}
	};

	return (
		<>
			{!initSuccess ? (
				<Form>
					<FormGroup>
						<Label for="register-email">Email</Label>
						<Input
							type="email"
							name="email"
							id="register-email"
							onChange={(e) => setEmail(e.target.value)}
						/>
					</FormGroup>
					<FormGroup>
						<Label for="register-name">Name</Label>
						<Input
							type="text"
							name="name"
							id="register-name"
							onChange={(e) => setName(e.target.value)}
						/>
					</FormGroup>
					<Button color="primary" onClick={register}>
						Submit
					</Button>
					{error && (
						<Alert color="danger" style={{ marginTop: 10 }}>
							Error: Could not register
						</Alert>
					)}
				</Form>
			) : (
				<ChatForm conversation_id={conversationId} />
			)}
		</>
	);
};

export default RegisterForm;
