import React from "react";
import { Container, Row, Col } from "reactstrap";
import "bootstrap/dist/css/bootstrap.css";

import RegisterForm from "./components/RegisterForm";
import { SContainer, SFormContainer } from "./styles";

const App = () => {
	return (
		<div>
			<SContainer>
				<Row>
					<Col sm="12" md={{ size: 5, offset: 4 }}>
						<h1>Chatbot</h1>
						<SFormContainer>
							<RegisterForm />
						</SFormContainer>
					</Col>
				</Row>
			</SContainer>
		</div>
	);
};

export default App;
